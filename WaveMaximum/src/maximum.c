#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void WaveMaximum_Maximum(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;

  int max_handle = CCTK_ReductionHandle("maximum");
  if(max_handle < 0) {
    CCTK_VERROR("Could not get maximum reduction handle: %d", max_handle);
  }

  int varidx = CCTK_VarIndex("WaveToy::phi");
  if(varidx < 0) {
    CCTK_VERROR("Could not get varindex for WaveToy::phi: %d", varidx);
  }

  // see https://www.einsteintoolkit.org/usersguide/UsersGuidech9.html#x13-118000C1.9
  int ierr = CCTK_Reduce(cctkGH,
    -1,         /* make result available to all procs */
    max_handle, /* reduction op to perform */
    1,          /* number of outputs (usually same as inputs) */
    CCTK_VARIABLE_REAL, /* output type for all outputs */
    maxval,     /* pointer to ouptut */
    1,          /* 1 input grid variables (functions, scalars or or arrays) */
    varidx      /* the variable indices of the grid variables, as with printf */
  );
  if(ierr < 0) {
    CCTK_VERROR("Reduction failed: %d", ierr);
  }

  CCTK_VINFO("Found maxval of %g", (double)*maxval);
}
