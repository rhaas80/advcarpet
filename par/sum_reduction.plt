#!/usr/bin/gnuplot -persist

set noxtics
set term "pdf" size 5./4.,3./4.

# the weights
set ytics 0.5 format ""
set xlabel "weight"

set output "weight_rl0.pdf"
plot [-0.55:0.55] "gaussian-weights/weight.x.asc" u 10:($3==0 ? $13 : 1/0)  w boxes fill solid not

set output "weight_rl1.pdf"
plot [-0.55:0.55] "gaussian-weights/weight.x.asc" u 10:($3==1 ? $13 : 1/0)  w boxes fill solid not

# phi
set ytics 0.5 format ""
set xlabel "phi"

set output "phi_rl0.pdf"
plot [-0.55:0.55] "gaussian-weights/phi.x.asc" u 10:($3==0 ? $13 : 1/0)  w boxes fill solid not

set output "phi_rl1.pdf"
plot [-0.55:0.55] "gaussian-weights/phi.x.asc" u 10:($3==1 ? $13 : 1/0)  w boxes fill solid not
