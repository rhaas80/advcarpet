#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "cctk_Schedule.h"

#include "carpet.hh"

#include <cstdio>

// a helper to output the current schedule information
static void say_hello(const cGH *cctkGH, const char *hello)
{
  CCTK_INT rank = CCTK_MyProc(cctkGH);
  CCTK_INT reflevel = GetRefinementLevel(cctkGH);
  CCTK_INT reflevels = GetRefinementLevels(cctkGH);
  CCTK_INT maxreflevels = GetMaxRefinementLevels(cctkGH);
  CCTK_INT map = reflevel >= 0 ? GetMap(cctkGH) : -1;
  CCTK_INT maps = GetMaps(cctkGH);
  CCTK_INT local_component = map >= 0 ? GetLocalComponent(cctkGH) : -1;
  CCTK_INT local_components = map >= 0 ? GetLocalComponents(cctkGH) : -1;
  CCTK_INT timelevel = GetTimeLevel(cctkGH);
  const cFunctionData *current_function = CCTK_ScheduleQueryCurrentFunction(cctkGH);

  // I use fprintf to ensure that all ranks print this
  fprintf(stderr, "[rank %d] in routine %s:%s of %s on refinement level %d "
          "of %d (of a possible total of %d) on local component %d of %d "
          "on map %d of %d on timelevel %d: %s\n",
          rank, current_function->where, current_function->routine,
          current_function->thorn, reflevel, reflevels, maxreflevels,
          local_component, local_components, map, maps, timelevel, hello);
}

extern "C"
void RescaleMacros_RescaleMaximum(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;

  CCTK_REAL maxval = -1.;

  // put this in a block so that the compiler does not warn about multiple
  // DECLARE_CCTK_ARGUMENTS
  {
    DECLARE_CCTK_ARGUMENTS;

    say_hello(cctkGH, "computing maximum");

    int max_handle = CCTK_ReductionHandle("maximum");
    if(max_handle < 0) {
      CCTK_VERROR("Could not get maximum reduction handle: %d", max_handle);
    }

    int varidx = CCTK_VarIndex("WaveToy::phi");
    if(varidx < 0) {
      CCTK_VERROR("Could not get varindex for WaveToy::phi: %d", varidx);
    }

    // see https://www.einsteintoolkit.org/usersguide/UsersGuidech9.html#x13-118000C1.9
    int ierr = CCTK_Reduce(cctkGH,
      -1,         /* make result available to all procs */
      max_handle, /* reduction op to perform */
      1,          /* number of outputs (usually same as inputs) */
      CCTK_VARIABLE_REAL, /* output type for all outputs */
      &maxval,    /* pointer to ouptut */
      1,          /* 1 input grid variables (functions, scalars or or arrays) */
      varidx      /* the variable indices of the grid variables, as with printf */
    );
    if(ierr < 0) {
      CCTK_VERROR("Reduction failed: %d", ierr);
    }

    CCTK_VINFO("Found maxval of %g", (double)maxval);
  }

  // this is my very own LOOP-LOCAL option
  // called on every single reflevel
  BEGIN_REFLEVEL_LOOP(cctkGH) {
    BEGIN_LOCAL_MAP_LOOP(cctkGH, CCTK_GF) {
      BEGIN_LOCAL_COMPONENT_LOOP(cctkGH, CCTK_GF) {
        DECLARE_CCTK_ARGUMENTS;
        say_hello(cctkGH, "rscaling values");

        CCTK_REAL scaleby = amplitude / maxval;
        CCTK_LOOP3_ALL(rescale, cctkGH, i,j,k) {
          ptrdiff_t idx = CCTK_GFINDEX3D(cctkGH, i,j,k);
          phi[idx] *= scaleby;
        } CCTK_ENDLOOP3_ALL(rescale);
      } END_LOCAL_COMPONENT_LOOP;
    } END_LOCAL_MAP_LOOP;
  } END_REFLEVEL_LOOP;
}
