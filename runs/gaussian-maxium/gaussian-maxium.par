############################################################
#
# gaussian.par
#
# A simple example parameter file for WaveToyC
#
############################################################

ActiveThorns = "CoordBase SymBase NaNChecker CartGrid3D Boundary IOUtil Time InitBase"
ActiveThorns = "Carpet CarpetLib CarpetReduce CarpetRegrid2 CarpetIOASCII CarpetIOHDF5 CarpetIOBasic CarpetIOScalar"
ActiveThorns = "IDScalarWaveC WaveToyC"
ActiveThorns = "WaveMaximum"

cactus::cctk_itlast = 2

idscalarwave::initial_data = "gaussian"

wavetoy::bound = "flat"

CartGrid3D::domain = "full"
CartGrid3D::type = "coordbase"
CartGrid3D::avoid_origin = "no"

CoordBase::domainsize = "minmax"
CoordBase::xmin = -0.5
CoordBase::ymin = -0.5
CoordBase::zmin = -0.5
CoordBase::xmax = +0.5
CoordBase::ymax = +0.5
CoordBase::zmax = +0.5
CoordBase::dx   =  0.02
CoordBase::dy   =  0.02
CoordBase::dz   =  0.02

CoordBase::boundary_size_x_lower     = 2
CoordBase::boundary_size_y_lower     = 2
CoordBase::boundary_size_z_lower     = 2
CoordBase::boundary_size_x_upper     = 2
CoordBase::boundary_size_y_upper     = 2
CoordBase::boundary_size_z_upper     = 2

time::dtfac = 0.5

carpet::max_refinement_levels = 2
carpet::domain_from_coordbase = yes
carpet::ghost_size = 2
carpet::prolongation_order_space = 3
carpet::prolongation_order_time = 2
carpet::init_fill_timelevels = yes

Carpet::verbose           = no
Carpet::veryverbose       = no
Carpet::schedule_barriers = no
Carpet::storage_verbose   = no
Carpet::recompose_verbose = no
CarpetLib::verbose        = no

CarpetRegrid2::verbose     = yes
CarpetRegrid2::num_centres = 1
CarpetRegrid2::freeze_unaligned_levels = yes

# this puts the boxes in the xy plane, but since Carpet predominatly splits in
# the z direction this gives misleading plots
CarpetRegrid2::num_levels_1         =  2
CarpetRegrid2::position_x_1         =  0.
CarpetRegrid2::position_y_1         =  0.
CarpetRegrid2::position_z_1         =  0.
CarpetRegrid2::radius_1[ 1]         =  0.125

ioscalar::outScalar_every = 1
ioscalar::outScalar_reductions = "maximum sum isum"
ioscalar::outScalar_vars = "wavetoy::phi"

iobasic::outInfo_every = 1
iobasic::outInfo_vars = "wavetoy::phi wavemaximum::maxval"

ioascii::out0D_every = 2
ioascii::out0D_vars = "wavemaximum::maxval"

ioascii::out1D_every = 2
ioascii::out1D_d = "no"
ioascii::out1D_y = "no"
ioascii::out1D_vars = "wavetoy::phi"

iohdf5::out3d_vars = "wavetoy::phi"
iohdf5::out3d_every = 1
iohdf5::out3d_ghosts = "no"

IO::out_dir = $parfile
