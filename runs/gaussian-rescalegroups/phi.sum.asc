# Scalar ASCII output created by CarpetIOScalar
# created on fdea4908.spdns.org by rhaas on Sep 10 2018 at 22:24:09+0100
# parameter filename: "../par/gaussian-rescalegroups.par"
#
# WAVETOY::phi (phi)
# 1:iteration 2:time 3:data
0 0 349.365503167897
2 0.0025 237.82132948234
4 0.005 125.995410017959
6 0.0075 15.3536872947862
8 0.01 -123.620751605989
10 0.0125 -241.264310914064
12 0.015 -358.491762244011
14 0.0175 -475.208211919515
16 0.02 -580.213552179014
