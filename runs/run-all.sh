mpirun -n 2 ../../../exe/cactus_sim ../par/gaussian-rescalegroups.par &>gaussian-rescalegroups.log
mpirun -n 2 ../../../exe/cactus_sim ../par/gaussian-rescalemacros.par &>gaussian-rescalemacros.log
mpirun -n 1 ../../../exe/cactus_sim ../par/gaussian-maxium.par &>gaussian-maxium.log
mpirun --oversubscribe -n 3 ../../../exe/cactus_sim ../par/gaussian-fmr.par &>gaussian-fmr.log
mpirun -n 2 ../../../exe/cactus_sim ../par/gaussian-simple.par &>gaussian-simple.log 
mpirun -n 1 ../../../exe/cactus_sim ../par/gaussian-weights.par &>gaussian-weights.log 
mpirun -n 1 ../../../exe/cactus_sim ../par/modesexample.par &>modesexample.log
